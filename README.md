# HabboPHP

Discontinued project. Please go on and search for community alternatives.

**WARNING !**

Use extra caution when deploying HabboPHP to production environments due to old code (and mostly not maintained) that can expose CSRF/XSS vulnerabilities. Please double check before really using it.

Rework of the CMS on Laravel 7.x (and maybe 8.x) : https://github.com/notaryzw3b/habbophp-renew

Thank you for still caring about our wonderful (old) project ;)